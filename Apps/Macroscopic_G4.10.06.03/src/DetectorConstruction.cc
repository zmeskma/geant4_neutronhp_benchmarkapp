//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction class
//
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4RunManager.hh"

#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4SDParticleWithEnergyFilter.hh"
#include "G4PSCellFlux.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include <array>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
:G4VUserDetectorConstruction(),
 fPSphere(0), fLSphere(0), fMaterial(0), fDetectorMessenger(0)
{
  fSphereSize = 30*cm;
  DefineMaterials();
  SetMaterial("Water_TS");
  fDetectorMessenger = new DetectorMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ delete fDetectorMessenger;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  return ConstructVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{
  G4int ncomponents,natoms;

  // Light Water
  G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Element* H_water  = new G4Element("TS_H_of_Water" ,"H_water" , ncomponents=1);
  H_water->AddIsotope(H1, 100*perCent);
  G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  O->AddIsotope(O16, 100*perCent);
  G4Material* H2O = new G4Material("Water_TS", 0.9955*g/cm3, ncomponents=2);
  H2O->AddElement(H_water, natoms=2);
  H2O->AddElement(O, natoms=1);

  // Heavy Water
  G4Isotope* H2 = new G4Isotope("H2",1,2, 2.0141*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Element* D_Hwater  = new G4Element("TS_D_of_Heavy_Water", "D_Hwater", ncomponents=1);
  D_Hwater->AddIsotope(H2, 100*perCent);
  //G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  //O->AddIsotope(O16, 100*perCent);
  G4Material* D2O = new G4Material("Heavy_Water_TS", 1.11*g/cm3, ncomponents=2);
  D2O->AddElement(D_Hwater, natoms=2);
  D2O->AddElement(O, natoms=1);

  // Heavy Water with TSL oxygen - not in ENDF\B-VII.1, not yet implemented in G4ParticleHPThermalScatteringNames
  //G4Isotope* H2 = new G4Isotope("H2",1,2, 2.0141*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  //G4Element* D_Hwater  = new G4Element("TS_D_of_Heavy_Water", "D_Hwater", ncomponents=1);
  //D_Hwater->AddIsotope(H2, 100*perCent);
  G4Element* O_Hwater = new G4Element("TS_O_of_Heavy_Water","O_Hwater",ncomponents=1);
  O_Hwater->AddIsotope(O16, 100*perCent);
  G4Material* D2O_wO = new G4Material("Heavy_Water_TS", 1.11*g/cm3, ncomponents=2);
  D2O_wO->AddElement(D_Hwater, natoms=2);
  D2O_wO->AddElement(O_Hwater, natoms=1);

  // Para Hydrogen
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  G4Element* H_para  = new G4Element("TS_H_of_Para_Hydrogen" ,"H_para" , ncomponents=1);
  H_para->AddIsotope(H1, 100*perCent);
  G4Material* para_hydrogen =
  new G4Material("Para_Hydrogen_TS", 0.07085*g/cm3,ncomponents=1,kStateLiquid,20*kelvin,1*atmosphere);
  para_hydrogen->AddElement(H_para, natoms=2);

  // Ortho Hydrogen
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  G4Element* H_ortho  = new G4Element("TS_H_of_Ortho_Hydrogen" ,"H_ortho" , ncomponents=1);
  H_ortho->AddIsotope(H1, 100*perCent);
  G4Material* ortho_hydrogen =
  new G4Material("Ortho_Hydrogen_TS", 0.07085*g/cm3, ncomponents=1,kStateLiquid,20*kelvin,1*atmosphere);
  ortho_hydrogen->AddElement(H_ortho, natoms=2);

  // Para Deuterium
  //G4Isotope* H2 = new G4Isotope("H2",1,2, 2.0141*g/mole);
  G4Element* D_para  = new G4Element("TS_D_of_Para_Deuterium", "D_para", ncomponents=1);
  D_para->AddIsotope(H2, 100*perCent);
  G4Material* para_deuterium =
  new G4Material("Para_Deuterium_TS", 0.07085*g/cm3, ncomponents=1,kStateLiquid,20*kelvin,1*atmosphere);
  para_deuterium->AddElement(D_para, natoms=2);

  // Ortho Deuterium
  //G4Isotope* H2 = new G4Isotope("H2",1,2, 2.0141*g/mole);
  G4Element* D_ortho  = new G4Element("TS_D_of_Ortho_Deuterium", "D_ortho", ncomponents=1);
  D_ortho->AddIsotope(H2, 100*perCent);
  G4Material* ortho_deuterium =
  new G4Material("Ortho_Deuterium_TS", 0.07085*g/cm3, ncomponents=1,kStateLiquid,20*kelvin,1*atmosphere);
  ortho_deuterium->AddElement(D_ortho, natoms=2);

  // Ice - not in ENDF\B-VII.1
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Element* H_ice  = new G4Element("TS_H_of_Ice" ,"H_ice" , ncomponents=1);
  H_ice->AddIsotope(H1, 100*perCent);
  //G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  //O->AddIsotope(O16, 100*perCent);
  G4Material* ice = new G4Material("Ice_TS", 0.917*g/cm3, ncomponents=2, kStateSolid, 273.15*kelvin);
  ice->AddElement(H_ice, natoms=2);
  ice->AddElement(O, natoms=1);

  // Ice_wO - only in ENDF\B-VIII.0
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  //G4Element* H_ice  = new G4Element("TS_H_of_Ice" ,"H_ice" , ncomponents=1);
  //H_ice->AddIsotope(H1, 100*perCent);
  G4Element* O_ice = new G4Element("TS_O_of_Ice","O_ice",ncomponents=1);
  O_ice->AddIsotope(O16, 100*perCent);
  G4Material* ice_wO = new G4Material("Ice_wO_TS", 0.917*g/cm3, ncomponents=2, kStateSolid, 273.15*kelvin);
  ice_wO->AddElement(H_ice, natoms=2);
  ice_wO->AddElement(O_ice, natoms=1);

  // Graphite
  G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* C_graphite   = new G4Element("TS_C_of_Graphite","C", ncomponents=1);
  C_graphite->AddIsotope(C12, 100.*perCent);
  G4Material* graphite = new G4Material("Graphite_TS", 2.23*g/cm3, ncomponents=1);
  graphite->AddElement(C_graphite, natoms=1);

  // Graphite porosity 10 percent - only ENDF\B-VIII.0
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* C_graphite_10p   = new G4Element("TS_C_of_Graphite_Porosity_10percent","C_10p", ncomponents=1);
  C_graphite_10p->AddIsotope(C12, 100.*perCent);
  G4Material* graphite_10p = new G4Material("Graphite_TS", 2.23*g/cm3, ncomponents=1);
  graphite_10p->AddElement(C_graphite_10p, natoms=1);

  // Graphite porosity 30 percent - only ENDF\B-VIII.0
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* C_graphite_30p   = new G4Element("TS_C_of_Graphite_Porosity_30percent","C_30p", ncomponents=1);
  C_graphite_30p->AddIsotope(C12, 100.*perCent);
  G4Material* graphite_30p = new G4Material("Graphite_TS", 2.23*g/cm3, ncomponents=1);
  graphite_30p->AddElement(C_graphite_30p, natoms=1);

  // Polyethylene
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* H_polyethylene  = new G4Element("TS_H_of_Polyethylene" ,"H_polyethylene" , ncomponents=1);
  H_polyethylene->AddIsotope(H1, 100.*perCent);
  G4Element* C = new G4Element("Carbon","C", ncomponents=1);
  C->AddIsotope(C12, 100.*perCent);
  G4Material* polyethylene = new G4Material("Polyethylene_TS", 0.94*g/cm3, 2);
  polyethylene->AddElement(C, natoms=1);
  polyethylene->AddElement(H_polyethylene, natoms=2);

  // Liquid Methane - not in JEFF3.3
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* H_lmethane  = new G4Element("TS_H_of_Liquid_Methane" ,"H_lmethane" , ncomponents=1);
  H_lmethane->AddIsotope(H1, 100*perCent);
  //G4Element* C = new G4Element("Carbon","C", ncomponents=1);
  //C->AddIsotope(C12, 100.*perCent);
  G4Material* liquid_methane = new G4Material("Liquid_Methane_TS", 0.422*g/cm3, ncomponents=2, kStateLiquid);
  liquid_methane->AddElement(H_lmethane, natoms=4);
  liquid_methane->AddElement(C, natoms=1);

  // Solid Methane - only in ENDF/B-VIII.0
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* H_smethane  = new G4Element("TS_H_of_Solid_Methane" ,"H_smethane" , ncomponents=1);
  H_smethane->AddIsotope(H1, 100*perCent);
  //G4Element* C = new G4Element("Carbon","C", ncomponents=1);
  //C->AddIsotope(C12, 100.*perCent);
  G4Material* solid_methane = new G4Material("Solid_Methane_TS", 0.516*g/cm3, ncomponents=2, kStateSolid);
  solid_methane->AddElement(H_smethane, natoms=4);
  solid_methane->AddElement(C, natoms=1);

  // Benzene - not in JEFF3.3, not yet implemented in G4ParticleHPThermalScatteringNames
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* H_benzene  = new G4Element("TS_H_of_Benzene" ,"H_benzene" , ncomponents=1);
  H_benzene->AddIsotope(H1, 100*perCent);
  //G4Element* C = new G4Element("Carbon","C", ncomponents=1);
  //C->AddIsotope(C12, 100.*perCent);
  G4Material* benzene = new G4Material("Benzene_TS", 0.8765*g/cm3, ncomponents=2);
  benzene->AddElement(H_benzene, natoms=6);
  benzene->AddElement(C, natoms=6);

  // Toluene - only in JEFF3.3
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* H_toluene  = new G4Element("TS_H_of_Toluene" ,"H_toluene" , ncomponents=1);
  H_toluene->AddIsotope(H1, 100*perCent);
  //G4Element* C = new G4Element("Carbon","C", ncomponents=1);
  //C->AddIsotope(C12, 100.*perCent);
  G4Material* toluene = new G4Material("Toluene_TS", 0.8669*g/cm3, ncomponents=2);
  toluene->AddElement(H_toluene, natoms=8);
  toluene->AddElement(C, natoms=7);

  // Mesitylene-phaseII - only in JEFF3.3
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Element* H_mesitylene  = new G4Element("TS_H_of_Mesitylene_phaseII" ,"H_mesitylene" , ncomponents=1);
  H_mesitylene->AddIsotope(H1, 100*perCent);
  //G4Element* C = new G4Element("Carbon","C", ncomponents=1);
  //C->AddIsotope(C12, 100.*perCent);
  G4Material* mesitylene = new G4Material("Mesitylene_TS", 0.8637*g/cm3, ncomponents=2);
  mesitylene->AddElement(H_mesitylene, natoms=12);
  mesitylene->AddElement(C, natoms=9);

  // PolymethylMethacrylate, Lucite, C5O2H8 - only in ENDF/B-VIII.0
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Element* H_polymethylmethacrylate  = new G4Element("TS_H_of_PolymethylMethacrylate" ,"H_polymethylmethacrylate" , 1.);
  H_polymethylmethacrylate->AddIsotope(H1, 100.*perCent);
  //G4Element* C = new G4Element("Carbon","C", 1);
  //C->AddIsotope(C12, 100.*perCent);
  //G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  //O->AddIsotope(O16, 100*perCent);
  G4Material* polymethylmethacrylate = new G4Material("PolymethylMethacrylate_TS", 1.19*g/cm3, 3);
  polymethylmethacrylate->AddElement(C, natoms=5);
  polymethylmethacrylate->AddElement(O, natoms=2);
  polymethylmethacrylate->AddElement(H_polymethylmethacrylate, natoms=8);

  // Calcium hydride CaH2 - only in JEFF3.3 not yet impemented in G4ParticleHPThermalScatteringNames
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  G4Isotope* Ca20 = new G4Isotope("Ca20", 20, 40, 39.9626*g/mole);
  G4Element* H_cah2  = new G4Element("TS_H_of_Calcium_Hydride" ,"H_cah2" , ncomponents=1);
  H_cah2->AddIsotope(H1, 100*perCent);
  G4Element* Ca_cah2 = new G4Element("TS_Ca_of_Calcium_Hydride","Ca_cah2", ncomponents=1);
  Ca_cah2->AddIsotope(Ca20, 100.*perCent);
  G4Material* calcium_hydride = new G4Material("Calcium_Hydride_TS", 1.7*g/cm3, ncomponents=2);
  calcium_hydride->AddElement(H_cah2, natoms=2);
  calcium_hydride->AddElement(Ca_cah2, natoms=1);

  // Yttrium Hydride YH2 - only in ENDF\B-VIII.0
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  G4Isotope* Y89 = new G4Isotope("Y89", 39, 89, 88.9058*g/mole);
  G4Element* H_yh2  = new G4Element("TS_H_of_YH2" ,"H_yh2" , ncomponents=1);
  H_yh2->AddIsotope(H1, 100*perCent);
  G4Element* Y_yh2 = new G4Element("TS_Y_of_YH2","Y_yh2", ncomponents=1);
  Y_yh2->AddIsotope(Y89, 100.*perCent);
  G4Material* yttrium_hydride = new G4Material("Yttrium_Hydride_TS", 4.2*g/cm3, ncomponents=2);
  yttrium_hydride->AddElement(H_yh2, natoms=2);
  yttrium_hydride->AddElement(Y_yh2, natoms=1);

  // Be metal
  G4Isotope* Be9 = new G4Isotope("Be9", 4, 9,  9.0121*g/mole);
  G4Element* Be   = new G4Element("TS_Beryllium_Metal","Be", ncomponents=1);
  Be->AddIsotope(Be9, 100.*perCent);
  G4Material* beryllium = new G4Material("Beryllium_TS", 1.85*g/cm3, 1);
  beryllium->AddElement(Be, natoms=1);

  // Mg magnesium - only in JEFF3.3 not yet implemented in G4ParticleHPThermalScatteringNames
  G4Isotope* Mg24 = new G4Isotope("Mg24", 12, 24, 23.9850*g/mole);
  G4Isotope* Mg25 = new G4Isotope("Mg25", 12, 25, 24.9858*g/mole);
  G4Isotope* Mg26 = new G4Isotope("Mg26", 12, 26, 26.9815*g/mole);
  G4Element* Mg24_magnesium   = new G4Element("TS_Mg24_of_Magnesium","Mg24_magnesium", ncomponents=1);
  Mg24_magnesium->AddIsotope(Mg24, 100.*perCent);
  G4Element* Mg25_magnesium   = new G4Element("TS_Mg25_of_Magnesium","Mg25_magnesium", ncomponents=1);
  Mg25_magnesium->AddIsotope(Mg25, 100.*perCent);
  G4Element* Mg26_magnesium   = new G4Element("TS_Mg26_of_Magnesium","Mg26_magnesium", ncomponents=1);
  Mg26_magnesium->AddIsotope(Mg26, 100.*perCent);
  G4Material* magnesium = new G4Material("Magnesium_TS", 1.74*g/cm3, ncomponents=3);
  magnesium->AddElement(Mg24_magnesium, natoms=7899);
  magnesium->AddElement(Mg25_magnesium, natoms=1000);
  magnesium->AddElement(Mg26_magnesium, natoms=1101);

  // Al metal - not in JEFF3.3
  G4Isotope* Al27 = new G4Isotope("Al27", 13, 27, 26.9815*g/mole);
  G4Element* Al   = new G4Element("TS_Aluminium_Metal","Al", ncomponents=1);
  Al->AddIsotope(Al27, 100.*perCent);
  G4Material* aluminium = new G4Material("Aluminium_TS", 2.699*g/cm3, ncomponents=1);
  aluminium->AddElement(Al, natoms=1);

  // Iron - not in JEFF3.3
  G4Isotope* Fe56 = new G4Isotope("Fe56", 26, 56,  55.9349*g/mole);
  G4Element* Fe  = new G4Element("TS_Iron_Metal" ,"Fe" , ncomponents=1);
  Fe->AddIsotope(Fe56, 100.*perCent);
  G4Material* iron = new G4Material("Iron", 7.874*g/cm3, ncomponents=1);
  iron->AddElement(Fe, natoms=1);

  // BeO - not in JEFF3.3
  //G4Isotope* Be9 = new G4Isotope("Be9", 4, 9,  9.0121*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Element* Be_oxide   = new G4Element("TS_Be_of_Beryllium_Oxide","Be_oxide", ncomponents=1);
  Be_oxide->AddIsotope(Be9, 100.*perCent);
  G4Element* O_Beoxide   = new G4Element("TS_O_of_Beryllium_Oxide","O_Beoxide", ncomponents=1);
  O_Beoxide->AddIsotope(O16, 100.*perCent);
  G4Material* beryllium_oxide = new G4Material("Beryllium_Oxide_TS", 3.01*g/cm3, ncomponents=2);
  beryllium_oxide->AddElement(Be_oxide, natoms=1);
  beryllium_oxide->AddElement(O_Beoxide, natoms=1);

  // Al2O3 - sapphire - only in JEFF3.3
  //G4Isotope* Al27 = new G4Isotope("Al27", 13, 27, 26.9815*g/mole);
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Element* Al_sapphire   = new G4Element("TS_Al_of_Sapphir","Al_sapphire", ncomponents=1);
  Al_sapphire->AddIsotope(Al27, 100.*perCent);
  G4Element* O_sapphire   = new G4Element("TS_O_of_Sapphir","O_sapphire", ncomponents=1);
  O_sapphire->AddIsotope(O16, 100.*perCent);
  G4Material* sapphire = new G4Material("Sapphire_TS", 3.97*g/cm3, ncomponents=2);
  sapphire->AddElement(Al_sapphire, natoms=2);
  sapphire->AddElement(O_sapphire, natoms=3);

  //Silicon Carbide SiC - only in ENDF\B-VIII.0
  //G4Isotope* C12 = new G4Isotope("C12", 6, 12, 12.0*g/mole);
  G4Isotope* Si28 = new G4Isotope("Si28", 14, 28, 27.9769*g/mole);
  G4Isotope* Si29 = new G4Isotope("Si29", 14, 29, 28.9765*g/mole);
  G4Isotope* Si30 = new G4Isotope("Si30", 14, 30, 29.9738*g/mole);
  G4Element* C_sic   = new G4Element("TS_C_of_SiC","C_sic", ncomponents=1);
  C_sic->AddIsotope(C12, 100.*perCent);
  G4Element* Si28_sic   = new G4Element("TS_Si28_of_SiC","Si28_sic", ncomponents=1);
  Si28_sic->AddIsotope(Si28, 100.*perCent);
  G4Element* Si29_sic   = new G4Element("TS_Si29_of_SiC","Si29_sic", ncomponents=1);
  Si29_sic->AddIsotope(Si29, 100.*perCent);
  G4Element* Si30_sic   = new G4Element("TS_Si30_of_SiC","Si30_sic", ncomponents=1);
  Si30_sic->AddIsotope(Si30, 100.*perCent);
  G4Material* silicon_carbide = new G4Material("Silicon_Carbide_TS", 3.21*g/cm3, ncomponents=4);
  silicon_carbide->AddElement(C_sic, 10000);
  silicon_carbide->AddElement(Si28_sic, 9222);
  silicon_carbide->AddElement(Si29_sic, 469);
  silicon_carbide->AddElement(Si30_sic, 409);

  //Silicon Oxide SiO2 - alpha - not in JEFF3.3
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  //G4Isotope* Si28 = new G4Isotope("Si28", 14, 28, 27.9769*g/mole);
  //G4Isotope* Si29 = new G4Isotope("Si29", 14, 29, 28.9765*g/mole);
  //G4Isotope* Si30 = new G4Isotope("Si30", 14, 30, 29.9738*g/mole);
  //G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  //O->AddIsotope(O16, 100*perCent);
  G4Element* Si28_sio2_a   = new G4Element("TS_Si28_of_SiO2_alpha","Si28_sio2_a", ncomponents=1);
  Si28_sio2_a->AddIsotope(Si28, 100.*perCent);
  G4Element* Si29_sio2_a   = new G4Element("TS_Si29_of_SiO2_alpha","Si29_sio2_a", ncomponents=1);
  Si29_sio2_a->AddIsotope(Si29, 100.*perCent);
  G4Element* Si30_sio2_a   = new G4Element("TS_Si30_of_SiO2_alpha","Si30_sio2_a", ncomponents=1);
  Si30_sio2_a->AddIsotope(Si30, 100.*perCent);
  G4Material* silicon_oxide_alpha = new G4Material("Silicon_Oxide_Alpha_TS", 2.648*g/cm3, ncomponents=4);
  silicon_oxide_alpha->AddElement(O, 20000);
  silicon_oxide_alpha->AddElement(Si28_sio2_a, 9222);
  silicon_oxide_alpha->AddElement(Si29_sio2_a, 469);
  silicon_oxide_alpha->AddElement(Si30_sio2_a, 409);

  //Silicon Oxide SiO2 - beta - only in ENDF\B-VIII.0
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  //G4Isotope* Si28 = new G4Isotope("Si28", 14, 28, 27.9769*g/mole);
  //G4Isotope* Si29 = new G4Isotope("Si29", 14, 29, 28.9765*g/mole);
  //G4Isotope* Si30 = new G4Isotope("Si30", 14, 30, 29.9738*g/mole);
  //G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  //O->AddIsotope(O16, 100*perCent);
  G4Element* Si28_sio2_b   = new G4Element("TS_Si28_of_SiO2_beta","Si28_sio2_b", ncomponents=1);
  Si28_sio2_b->AddIsotope(Si28, 100.*perCent);
  G4Element* Si29_sio2_b   = new G4Element("TS_Si29_of_SiO2_beta","Si29_sio2_b", ncomponents=1);
  Si29_sio2_b->AddIsotope(Si29, 100.*perCent);
  G4Element* Si30_sio2_b   = new G4Element("TS_Si30_of_SiO2_beta","Si30_sio2_b", ncomponents=1);
  Si30_sio2_b->AddIsotope(Si30, 100.*perCent);
  G4Material* silicon_oxide_beta = new G4Material("Silicon_Oxide_Beta_TS", 2.32*g/cm3, ncomponents=4);
  silicon_oxide_beta->AddElement(O, 20000);
  silicon_oxide_beta->AddElement(Si28_sio2_b, 9222);
  silicon_oxide_beta->AddElement(Si29_sio2_b, 469);
  silicon_oxide_beta->AddElement(Si30_sio2_b, 409);

  //Silicon Oxide SiO2 - singlecrystal - only in JEFF3.3, not yet implemented in G4ParticleHPThermalScatteringNames
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  //G4Isotope* Si28 = new G4Isotope("Si28", 14, 28, 27.9769*g/mole);
  //G4Isotope* Si29 = new G4Isotope("Si29", 14, 29, 28.9765*g/mole);
  //G4Isotope* Si30 = new G4Isotope("Si30", 14, 30, 29.9738*g/mole);
  //G4Element* O = new G4Element("Oxygen","O",ncomponents=1);
  //O->AddIsotope(O16, 100*perCent);
  G4Element* Si28_sio2_s   = new G4Element("TS_Si28_of_SiO2_singlecrystal","Si28_sio2_s", ncomponents=1);
  Si28_sio2_s->AddIsotope(Si28, 100.*perCent);
  G4Element* Si29_sio2_s   = new G4Element("TS_Si29_of_SiO2_singlecrystal","Si29_sio2_s", ncomponents=1);
  Si29_sio2_s->AddIsotope(Si29, 100.*perCent);
  G4Element* Si30_sio2_s   = new G4Element("TS_Si30_of_SiO2_singlecrystal","Si30_sio2_s", ncomponents=1);
  Si30_sio2_s->AddIsotope(Si30, 100.*perCent);
  G4Material* silicon_oxide_singlecrystal = new G4Material("Silicon_Oxide_Singlecrystal_TS", 2.648*g/cm3, ncomponents=4);
  silicon_oxide_singlecrystal->AddElement(O, 20000);
  silicon_oxide_singlecrystal->AddElement(Si28_sio2_s, 9222);
  silicon_oxide_singlecrystal->AddElement(Si29_sio2_s, 469);
  silicon_oxide_singlecrystal->AddElement(Si30_sio2_s, 409);

  // Zirconium Hydride - not in JEFF3.3
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  G4Isotope* Zr90 = new G4Isotope("Zr90", 40, 90, 89.9047*g/mole);
  G4Isotope* Zr91 = new G4Isotope("Zr91", 40, 91, 90.9056*g/mole);
  G4Isotope* Zr92 = new G4Isotope("Zr92", 40, 92, 91.9050*g/mole);
  G4Isotope* Zr94 = new G4Isotope("Zr94", 40, 94,  93.9063*g/mole);
  G4Isotope* Zr96 = new G4Isotope("Zr96", 40, 96,  95.9083*g/mole);
  G4Element* Zr90_zrh   = new G4Element("TS_Zr90_of_Zirconium_Hydride","Zr90_zrh", ncomponents=1);
  Zr90_zrh->AddIsotope(Zr90, 100.*perCent);
  G4Element* Zr91_zrh   = new G4Element("TS_Zr91_of_Zirconium_Hydride","Zr91_zrh", ncomponents=1);
  Zr91_zrh->AddIsotope(Zr91, 100.*perCent);
  G4Element* Zr92_zrh   = new G4Element("TS_Zr92_of_Zirconium_Hydride","Zr92_zrh", ncomponents=1);
  Zr92_zrh->AddIsotope(Zr92, 100.*perCent);
  G4Element* Zr94_zrh   = new G4Element("TS_Zr94_of_Zirconium_Hydride","Zr94_zrh", ncomponents=1);
  Zr94_zrh->AddIsotope(Zr94, 100.*perCent);
  G4Element* Zr96_zrh   = new G4Element("TS_Zr96_of_Zirconium_Hydride","Zr96_zrh", ncomponents=1);
  Zr96_zrh->AddIsotope(Zr96, 100.*perCent);
  G4Element* H_zirconium  = new G4Element("TS_H_of_Zirconium_Hydride" ,"H_zirconium" , ncomponents=1);
  H_zirconium->AddIsotope(H1, 100*perCent);
  G4Material* zirconium_hydride = new G4Material("Zirconium_Hydride_TS", 5.6*g/cm3, ncomponents=6);
  zirconium_hydride->AddElement(Zr90_zrh, 0.496096);
  zirconium_hydride->AddElement(Zr91_zrh, 0.109391);
  zirconium_hydride->AddElement(Zr92_zrh, 0.169045);
  zirconium_hydride->AddElement(Zr94_zrh, 0.175042);
  zirconium_hydride->AddElement(Zr96_zrh, 0.028801);
  zirconium_hydride->AddElement(H_zirconium, 0.021625);

  // Zirconium Hydride only H
  //G4Isotope* H1 = new G4Isotope("H1",1,1, 1.0078*g/mole);
  //G4Isotope* Zr90 = new G4Isotope("Zr90", 40, 90, 89.9047*g/mole);
  //G4Isotope* Zr91 = new G4Isotope("Zr91", 40, 91, 90.9056*g/mole);
  //G4Isotope* Zr92 = new G4Isotope("Zr92", 40, 92, 91.9050*g/mole);
  //G4Isotope* Zr94 = new G4Isotope("Zr94", 40, 94,  93.9063*g/mole);
  //G4Isotope* Zr96 = new G4Isotope("Zr96", 40, 96,  95.9083*g/mole);
  G4Element* elZr90   = new G4Element("Zirconium_90","elZr90", ncomponents=1);
  elZr90->AddIsotope(Zr90, 100.*perCent);
  G4Element* elZr91   = new G4Element("Zirconium_91","elZr91", ncomponents=1);
  elZr91->AddIsotope(Zr91, 100.*perCent);
  G4Element* elZr92   = new G4Element("Zirconium_92","elZr92", ncomponents=1);
  elZr92->AddIsotope(Zr92, 100.*perCent);
  G4Element* elZr94   = new G4Element("Zirconium_94","elZr94", ncomponents=1);
  elZr94->AddIsotope(Zr94, 100.*perCent);
  G4Element* elZr96   = new G4Element("Zirconium_96","elZr96", ncomponents=1);
  elZr96->AddIsotope(Zr96, 100.*perCent);
  //G4Element* H_zirconium  = new G4Element("TS_H_of_Zirconium_Hydride" ,"H_zirconium" , ncomponents=1);
  //H_zirconium->AddIsotope(H1, 100*perCent);
  G4Material* zirconium_hydride_onlyh = new G4Material("Zirconium_Hydride_onlyH_TS", 5.6*g/cm3, ncomponents=6);
  zirconium_hydride_onlyh->AddElement(elZr90, 0.496096);
  zirconium_hydride_onlyh->AddElement(elZr91, 0.109391);
  zirconium_hydride_onlyh->AddElement(elZr92, 0.169045);
  zirconium_hydride_onlyh->AddElement(elZr94, 0.175042);
  zirconium_hydride_onlyh->AddElement(elZr96, 0.028801);
  zirconium_hydride_onlyh->AddElement(H_zirconium, 0.021625);


  G4double enrichment = 0.03;
  G4double M_U235 = 235.0439301;
  G4double M_U238 = 238.0507884;
  G4double M_U=enrichment/M_U235+(1-enrichment)/M_U238;
  G4double a_U238_d=((1-enrichment)/M_U238*M_U)*10000;
  G4double a_U235_d=(1-a_U238_d)*10000;
  G4int a_U238=G4int(a_U238_d);
  G4int a_U235=G4int(a_U235_d);

  // UO2 - not in JEFF3.3
  //G4Isotope* O16 = new G4Isotope("O16", 8, 16, 15.9949*g/mole);
  G4Isotope* U235 = new G4Isotope("U235", 92, 235, 235.0439*g/mole);
  G4Isotope* U238 = new G4Isotope("U238", 92, 238, 238.0508*g/mole);
  G4Element* O_Udioxide   = new G4Element("TS_O_of_Uranium_Dioxide","O_Udioxide", ncomponents=1);
  O_Udioxide->AddIsotope(O16, 100.*perCent);
  G4Element* elU235_Udioxide   = new G4Element("TS_U235_of_Uranium_Dioxide","elU235_Udioxide", ncomponents=1);
  elU235_Udioxide->AddIsotope(U235, 100.*perCent);
  G4Element* elU238_Udioxide   = new G4Element("TS_U238_of_Uranium_Dioxide","elU238_Udioxide", ncomponents=1);
  elU238_Udioxide->AddIsotope(U238, 100.*perCent);
  G4Material* uranium_dioxide = new G4Material("Uranium_Dioxide_TS", 10.96*g/cm3, ncomponents=3);
  uranium_dioxide->AddElement(elU235_Udioxide, natoms=a_U235);
  uranium_dioxide->AddElement(elU238_Udioxide, natoms=a_U238);
  uranium_dioxide->AddElement(O_Udioxide, natoms=20000);

  // UN - only in ENDF\B-VIII.0
  G4Isotope* N14 = new G4Isotope("N14", 7, 14, 14.0031*g/mole);
  //G4Isotope* U235 = new G4Isotope("U235", 92, 235, 235.0439*g/mole);
  //G4Isotope* U238 = new G4Isotope("U238", 92, 238, 238.0508*g/mole);
  G4Element* N_Unitride   = new G4Element("TS_N_of_UN","N_Unitride", ncomponents=1);
  N_Unitride->AddIsotope(N14, 100.*perCent);
  G4Element* elU235_Unitride   = new G4Element("TS_U235_of_UN","elU235_Unitrid", ncomponents=1);
  elU235_Unitride->AddIsotope(U235, 100.*perCent);
  G4Element* elU238_Unitride   = new G4Element("TS_U238_of_UN","elU238_Unitride", ncomponents=1);
  elU238_Unitride->AddIsotope(U238, 100.*perCent);
  G4Material* uranium_nitride = new G4Material("Uranium_Nitride_TS", 10.96*g/cm3, ncomponents=3);
  uranium_nitride->AddElement(elU235_Unitride, natoms=a_U235);
  uranium_nitride->AddElement(elU238_Unitride, natoms=a_U238);
  uranium_nitride->AddElement(N_Unitride, natoms=10000);

/*
G4ParticleHPThermalScatteringNames::G4ParticleHPThermalScatteringNames()
{
names.insert ( std::pair < G4String , G4String > ( "TS_Aluminium_Metal" , "al_metal" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_Beryllium_Metal" , "be_metal" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_Be_of_Beryllium_Oxide" , "be_beo" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_C_of_Graphite" , "graphite" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_D_of_Heavy_Water" , "d_heavy_water" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Water" , "h_water" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Zirconium_Hydride" , "h_zrh" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Polyethylene" , "h_polyethylene" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_Iron_Metal" , "fe_metal" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_O_of_Uranium_Dioxide" , "o_uo2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_O_of_Beryllium_Oxide" , "o_beo" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_U_of_Uranium_Dioxide" , "u_uo2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_U235_of_Uranium_Dioxide" , "u235_uo2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_U238_of_Uranium_Dioxide" , "u238_uo2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_Zr_of_Zirconium_Hydride" , "zr_zrh" ) );      // ENDF-B71
names.insert ( std::pair < G4String , G4String > ( "TS_Zr90_of_Zirconium_Hydride" , "zr90_zrh" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Zr91_of_Zirconium_Hydride" , "zr91_zrh" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Zr92_of_Zirconium_Hydride" , "zr92_zrh" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Zr94_of_Zirconium_Hydride" , "zr94_zrh" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Zr96_of_Zirconium_Hydride" , "zr96_zrh" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Para_Hydrogen" , "h_para_h2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Ortho_Hydrogen" , "h_ortho_h2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_D_of_Para_Deuterium" , "d_para_d2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_D_of_Ortho_Deuterium" , "d_ortho_d2" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Liquid_Methane", "h_l_ch4" ) );
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Solid_Methane", "h_s_ch4" ) );

// 26/03/2021 - Added by L. Thulliez (CEA-Saclay)
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Ice", "h_ice" ) );                                  // ENDF-B80 & JEFF-3.3
names.insert ( std::pair < G4String , G4String > ( "TS_O_of_Ice", "o_ice" ) );                                  // ENDF-B80 & JEFF-3.3
names.insert ( std::pair < G4String , G4String > ( "TS_C_of_Graphite_Porosity_30percent" , "graphite_30p" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_C_of_Graphite_Porosity_10percent" , "graphite_10p" ) );  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_PolymethylMethacrylate", "h_c5o2h8" ) );            // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Al_of_Sapphir", "al_al2o3" ) );                          // JEFF-3.3
names.insert ( std::pair < G4String , G4String > ( "TS_O_of_Sapphir", "o_al2o3" ) );                            // JEFF-3.3
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Mesitylene_phaseII", "h_mesitylene_phaseII" ) );	   // JEFF-3.3
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_Toluene", "h_toluene" ) );                          // JEFF-3.3
names.insert ( std::pair < G4String , G4String > ( "TS_N_of_UN", "n_un" ) );                                    // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_U235_of_UN", "u235_un" ) );                              // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_U238_of_UN", "u238_un" ) );                              // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_H_of_YH2", "h_yh2" ) );                                  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Y_of_YH2", "y_yh2" ) );                                  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_C_of_SiC", "c_sic" ) );                                  // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si28_of_SiC", "si28_sic" ) );                            // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si29_of_SiC", "si29_sic" ) );                            // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si30_of_SiC", "si30_sic" ) );                            // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si28_of_SiO2_beta", "si28_sio2_beta" ) );                // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si29_of_SiO2_beta", "si29_sio2_beta" ) );                // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si30_of_SiO2_beta", "si30_sio2_beta" ) );                // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si28_of_SiO2_alpha", "si28_sio2_alpha" ) );              // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si29_of_SiO2_alpha", "si29_sio2_alpha" ) );              // ENDF-B80
names.insert ( std::pair < G4String , G4String > ( "TS_Si30_of_SiO2_alpha", "si30_sio2_alpha" ) );              // ENDF-B80
}
*/

 ///G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* DetectorConstruction::MaterialWithSingleIsotope( G4String name,
                           G4String symbol, G4double density, G4int Z, G4int A)
{
 // define a material from an isotope
 //
 G4int ncomponents;
 G4double abundance, massfraction;

 G4Isotope* isotope = new G4Isotope(symbol, Z, A);

 G4Element* element  = new G4Element(name, symbol, ncomponents=1);
 element->AddIsotope(isotope, abundance= 100.*perCent);

 G4Material* material = new G4Material(name, density, ncomponents=1);
 material->AddElement(element, massfraction=100.*perCent);

 return material;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::ConstructVolumes()
{
  // Cleanup old geometry
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();

  G4Sphere*
  sSphere = new G4Sphere("SphereSolid",                         //its name
                   0,fSphereSize,0*deg,360*deg,0*deg,180*deg);   //its dimensions

  fLSphere = new G4LogicalVolume(sSphere,                     //its shape
                             fMaterial,                 //its material
                             "SphereLogic");     //its name

  fPSphere = new G4PVPlacement(0,                          //no rotation
                            G4ThreeVector(),            //at (0,0,0)
                            fLSphere,                      //its logical volume
                            "SpherePlacement",       //its name
                            0,                          //its mother  volume
                            false,                      //no boolean operation
                            0);                         //copy number

  PrintParameters();

  //always return the root volume
  //
  return fPSphere;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::PrintParameters()
{
  G4cout << "\n The Sphere is of " << G4BestUnit(fSphereSize,"Length")
         << " of " << fMaterial->GetName()
         << "\n \n" << fMaterial << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::SetMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
     G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);

  if (pttoMaterial) {
    if(fMaterial != pttoMaterial) {
      fMaterial = pttoMaterial;
      if(fLSphere) { fLSphere->SetMaterial(pttoMaterial); }
      G4RunManager::GetRunManager()->PhysicsHasBeenModified();
    }
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found." << G4endl;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::SetSize(G4double value)
{
  fSphereSize = value;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
