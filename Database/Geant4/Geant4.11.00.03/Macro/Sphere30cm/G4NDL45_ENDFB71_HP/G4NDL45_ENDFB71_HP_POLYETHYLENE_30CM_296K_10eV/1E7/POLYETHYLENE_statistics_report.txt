The condition is: |relative difference|$\leq3.0\cdot \sigma+0.02$.

In comparison with T4 the condition is not met for 6 non-zero bins,                which is 1.993 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
0.000660693-0.000691831, 0.000831764-0.000870964, 0.00131826-0.00138038, 0.00218776-0.00229087, 0.00691831-0.00724436, 0.0158489-0.0165959.\\
The sum of squares (G4-T4)$^2$ is 4.497e-13. The sum of relative difference |G4-T4|/T4 is 7.441.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 1.256\%.

\vspace{5mm}

In comparison with M6 the condition is not met for 11 non-zero bins,                which is 3.654 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
1.31826e-05-1.38038e-05, 3.80189e-05-3.98107e-05, 0.000158489-0.000165959, 0.000831764-0.000870964, 0.00109648-0.00114815, 0.00131826-0.00138038, 0.00218776-0.00239883, 0.00436516-0.0047863, 0.00691831-0.00724436.\\
The sum of squares (G4-M6)$^2$ is 4.581e-13. The sum of relative difference |G4-M6|/M6 is 6.938.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 1.294\%.