The condition is: |relative difference|$\leq3.0\cdot \sigma+0.02$.

In comparison with T4 for energy the condition is not met for 25 non-zero bins,                which is 10.82 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
0.000158489-0.000165959, 0.000316228-0.000346737, 0.000501187-0.00057544, 0.001-0.00114815, 0.0020893-0.00239883, 0.00524807-0.0060256, 0.00870964-0.01, 0.020893-0.0251189, 0.0301995-0.0331131, 0.380189-0.398107.\\
The sum of squares (G4-T4)$^2$ is 1.113e+11. The sum of relative difference |G4-T4|/T4 is 14.92.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 1.246\%.

\vspace{5mm}

In comparison with M6 for energy the condition is not met for 23 non-zero bins,                which is 9.957 \% of all non-zero bins.\\
These are the ranges (in eV), where the condition is not met:\\
0.000158489-0.000165959, 0.000316228-0.000346737, 0.000524807-0.00057544, 0.001-0.00114815, 0.0020893-0.00239883, 0.00524807-0.0060256, 0.00870964-0.01, 0.020893-0.0251189, 0.0301995-0.0331131.\\
The sum of squares (G4-M6)$^2$ is 1.114e+11. The sum of relative difference |G4-M6|/M6 is 16.43.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 1.26\%.

In comparison with T4 for angle the condition is met for all bins.\\
The sum of squares (G4-T4)$^2$ is 3.958e+09. The sum of relative difference |G4-T4|/T4 is 1.433.\\
The weighted mean of relative difference |G4-T4|/T4 in non-zero bins is 0.7237\%.

\vspace{5mm}

In comparison with M6 for angle the condition is not met for 85 non-zero bins,                which is 42.5 \% of all non-zero bins.\\
These are the ranges (in $\mu$), where the condition is not met:\\
-0.8--0.79, -0.7--0.61, -0.58--0.56, -0.53--0.52, -0.49--0.45, -0.4--0.31, -0.23--0.18, -0.15--0.06, -0.01-0.05, 0.12-0.13, 0.14-0.15, 0.19-0.24, 0.3-0.33, 0.35-0.37, 0.4-0.41, 0.47-0.49, 0.55-0.57, 0.63-0.71, 0.76-0.77, 0.8-0.81, 0.82-0.84, 0.85-0.87, 0.88-0.9, 0.93-0.94, 0.95-1.\\
The sum of squares (G4-M6)$^2$ is 1.621e+11. The sum of relative difference |G4-M6|/M6 is 6.752.\\
The weighted mean of relative difference |G4-M6|/M6 in non-zero bins is 3.878\%.