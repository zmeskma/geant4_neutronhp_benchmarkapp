Application to benchmark the Geant4 Neutron-HP package.
It aims to test the neutron transport accuracy in the thermal energy region (meV-eV) in Geant4.

Description:
------------
The tests are done through comparison with neutron transport reference codes Tripoli-4 (version 11) and MCNP6.2 for materials gathered in a database. 
Two different benchmarks are performed [1]:
   * An integral benchmark / The sphere benchmark :
     Goal: benchmark the slowing down process of the neutrons in Geant4.
     Geometry: a homogeneous sphere with radius of 30 cm.
     Neutron source:  an isotropic 10 eV mono-energetic point source is placed at the sphere center. 
     Score: the neutron flux in the sphere. 
      
      
   * A microscopic benchmark / The thin cylinder benchmark :
     Goal: benchmark the nuclear data treatment in Geant4.
     Geometry: a thin cylinder with a length of 2 meters and a radius of 1 um, these dimensions ensures that at least one and only one neutron interaction happens in the cylinder.
     Neutron source: the neutrons have an energy equal to 0.01 eV and are shot parallel to the cylinder axis.
     Score: the energy and cosine of scattering angle of the scattered neutron. 


[1] L. Thulliez, C. Jouanne, E. Dumonteil, Improvement of Geant4 Neutron-HP package: From methodology to evaluated nuclear data library, Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment, Volume 1027, 2022, 166187, ISSN 0168-9002, https://doi.org/10.1016/j.nima.2021.166187.


Prerequisites:
--------------
1) Built and installed the Geant4 version that you want to test.
2) Download and unzip the NeutronHPBenchmarkApp directory.
3) Python3 requirements: numpy (handling data), matplotlib in version 3.6 or higher (creating graphs) and uproot. These can be installed by running a command "pip install -r requirements.txt" in ./NeutronHPBenchmarkApp/Apps/Scripts directory.
4) Latex requirements (to generate the benchmark summary): pdflatex and bibtex.


1) Build application and run simulations:
-----------------------------------------
a) Go to sub-directory ./NeutronHPBenchmarkApp/Scripts and prepare your runnerarguments.txt where the default values are set.
It is important to check the version, TSLreference, NeutronHPdatafile and TSLGeant4.
For description of the arguments run:
         python3 runner.py -h
b) Set the path in geant4.sh to the NeutronHPdatafile you declared as argument, set proper link for ThermalScattering directory you declared as argument.
c) Run the simulations for materials in database ./NeutronHPBenchmarkApp/Scripts sub-directory:
         python3 runner.py @runnerarguments.txt
         
If you want to run a subset of material change the file ./NeutronHPBenchmarkApp/Scripts/auxiliary/matarialdatabase.json

2) Generate the final report related to the benchmark:
------------------------------------------------------
a) Check arguments in ./NeutronHPBenchmarkApp/Scripts/processarguments.txt.
b) Run the processing script in in ./NeutronHPBenchmarkApp/Scripts sub-directory:
	    python3 process.py @processarguments.txt
c) The report is created in the sub-directory ./NeutronHPBenchmarkApp/Database/Geant4/Geant4.version/  
   Other graphs and statistics report will be saved to individual simulation directories.


Structure of the app:
*********************
./NeutronHPBenchmarkApp/Apps:
---------------------------
Macroscopic and microscopic applications to benchmark the Geant4 Neutron-HP package

./NeutronHPBenchmarkApp/Database:
---------------------------
There are sub-directories for each of three compared simulation programs Geant4, MCNP6.2 and Tripoli-4. 
In MCNP6.2 and Tripoli-4 sub-directories there are directly the Micro and Macro sub-directories with data for these benchmarks.
In Geant4 directory a sub-directory for a given version is created and inside them again the Macro and Micro sub-directories are created. In Macro subdirectory there is one more sub-directory for the radius of the sphere (all data are calculated for 30 cm to this end). Then inside of these directories a sub-directory for each of material from the database is created. There you can find results of the simulation in Results.root, output from simulation in output.txt, myrun.mac macro file with which the simulation was run, graphs for comparison of Geant4, Tripoli4 and MCNP6.2 and statistics report, which is also summerized in final report. The data for Geant4 version 11.00.03 calculated in G4NDL45_ENDFB71_HP and G4NDL47_ENDFB80_HP are already present.

./NeutronHPBenchmarkApp/Scripts:
--------------------------
In the main directory, there are the main run scripts runner.py and process.py with their argument files runnerarguments.txt and processarguments.txt.
In the subd-irectory modules, there are different modules used in process.py.
In sub-directory auxiliary, there are auxiliary files used in the main scripts, these are:
energyGroup_616.txt - default energy grouping for energy flux and energy of the scattered neutron
librarydatabase.json - database of libraries, which were input in runner and are stored here for each version of Geant4 to be used by process.py
materialdabase.json - database of materials, for which data calculated in Tripoli-4 and MCNP6.2 are present
references.bib - file with references for final report
