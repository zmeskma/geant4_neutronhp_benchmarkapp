import os
import json
import math
import sys
import argparse


parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-v","--version",default="11.00.03",
help="version of your Geant4")
parser.add_argument("-appv","--ApplicationVersion",default="11.00.03",choices=["10.06.03","11.00.03"],
help="version of the application, that should be used, currently there are two tested applications for versions 10.06.03, which should work for all versions 10, and 11.00.03, which should work for all versions 11 so far")
parser.add_argument("-r","--radius",default=30,type=int,
help="radius (in cm) of the sphere in Macroscopic benchmark")
parser.add_argument("-nEMac","--nEventMacro",default=1e7,type=int,help="number of particles in Macroscopic benchmark")
parser.add_argument("-nEMic","--nEventMicro",default=1e8,type=int,help="number of particles in Microscopic benchmark")
parser.add_argument("-tslref","--TSLreference",default="ENDFB71",choices=["ENDFB71","ENDFB80"],
help="thermal scattering library used in the reference code MCNP6.2 and Tripoli-4 for both energies above and under the thermal scattering threshold")
parser.add_argument("-nhp","--NeutronHPdatafile",default="G4NDL45",
choices=["G4NDL45","G4NDL46","G4NDL47","JEFF33","JEFF32","ENDFB80","ENDFB71","BROND31","JENDL4u"],
help="data file with Geant4 NeutronHP library, must be set in geant4.sh on your computer")
parser.add_argument("-tslG4","--TSLGeant4",default="ENDFB71_HP",
choices=["G4NDL45","G4NDL46","G4NDL47","JEFF33_ENDFB80_HP","JEFF33_ENDFB80_LP","JEFF33_HP","JEFF33_LP","ENDFB80_HP","ENDFB80_LP","ENDFB71_HP","ENDFB71_LP"],
help="thermal scattering library used in Geant4, ThermalScattering directory in NeutronHP directory set before on your computer")
parser.add_argument("-nT","--numberOfThreads",default=0,type=int,
help="number of threads used for simulation in Geant4, default 0 for automatic choice")
args=parser.parse_args()


scriptpath=os.path.abspath(os.path.dirname(__file__))
databasepath=scriptpath.replace("Scripts","Database")
applicationpath=scriptpath.replace("Scripts","Apps")
appversion=args.ApplicationVersion
version=args.version
TSLibrary=args.TSLreference
librarydir=args.NeutronHPdatafile+"_"+args.TSLGeant4
numofthreads=args.numberOfThreads

verboscmd="/run/verbose 2"
numofthreadscmd="/run/numberOfThreads "
detsizecmd="/testhadr/det/setSize "
detradiuscmd="/testhadr/det/setRadius "
detlengthcmd="/testhadr/det/setLength "
detmatcmd="/testhadr/det/setMatSpecial "
initializecmd="/run/initialize"
particlehpcmd1="/process/had/particle_hp/skip_missing_isotopes true"
particlehpcmd2="/process/had/particle_hp/do_not_adjust_final_state true"
runcmd1="/run/printProgress "
runcmd2="/run/beamOn "
delimiter="#\n"

if not os.path.exists(applicationpath+"/"+"Macroscopic"+"_G4."+version+"-build"):
    os.system("mkdir "+applicationpath+"/"+"Macroscopic"+"_G4."+version+"-build")
    os.chdir(applicationpath+"/"+"Macroscopic"+"_G4."+version+"-build")
    os.system("cmake ../Macroscopic_G4."+appversion)
    os.system("make")
else:
    os.chdir(applicationpath+"/"+"Macroscopic"+"_G4."+version+"-build")
    os.system("cmake ../Macroscopic_G4."+appversion)
    os.system("make")

if not os.path.exists(applicationpath+"/"+"Microscopic"+"_G4."+version+"-build"):
    os.system("mkdir "+applicationpath+"/"+"Microscopic"+"_G4."+version+"-build")
    os.chdir(applicationpath+"/"+"Microscopic"+"_G4."+version+"-build")
    os.system("cmake ../Microscopic_G4."+appversion)
    os.system("make")
else:
    os.chdir(applicationpath+"/"+"Microscopic"+"_G4."+version+"-build")
    os.system("cmake ../Microscopic_G4."+appversion)
    os.system("make")



with open(scriptpath+"/auxiliary/librarydatabase.json","r+") as jsonfile:
    if jsonfile.read(1):
        jsonfile.seek(0)
        database=json.load(jsonfile)
        if not(version in database.keys()):
            database[version]={}
            database[version][TSLibrary]=[librarydir]
        elif not(TSLibrary in database[version].keys()):
            database[version][TSLibrary]=[librarydir]
        elif not(librarydir in database[version][TSLibrary]):
            database[version][TSLibrary].append(librarydir)
        jsonfile.seek(0)
        jsonfile.truncate()
        json.dump(database,jsonfile, indent=2)
    else:
        database={}
        database[version]={}
        database[version][TSLibrary]=[librarydir]
        json.dump(database,jsonfile, indent=2)


with open(scriptpath+"/processarguments.txt", "w") as argfile:
    for key,value in vars(args).items():
        if key=="version" or key=="nEventMacro" or key=="nEventMicro" or key=="radius":
            argfile.write("--"+key+"\n"+str(value)+"\n")
    argfile.write("--relativelimit\n3\n")
    argfile.write("--absolutelimit\n0.02\n")

if not os.path.exists(databasepath+"/Geant4/Geant4."+version):
    os.system("mkdir "+databasepath+"/Geant4/Geant4."+version)
    os.system("mkdir "+databasepath+"/Geant4/Geant4."+version+"/Macro")
    os.system("mkdir "+databasepath+"/Geant4/Geant4."+version+"/Micro/")

for mac_or_mic in ["Macroscopic","Microscopic"]:
    if mac_or_mic=="Macroscopic":
        radius=args.radius
        databasekey="Sphere"+str(int(radius))+"cm"
        spheredir=databasekey+"/"
        beamon=args.nEventMacro
        gpscmd="""/gps/particle neutron
        /gps/pos/type Point
        /gps/pos/centre 0. 0. 0. cm
        /gps/ang/type iso
        /gps/ene/mono """
        if not os.path.exists(databasepath+"/Geant4/Geant4."+version+"/Macro/"+spheredir):
            os.system("mkdir "+databasepath+"/Geant4/Geant4."+version+"/Macro/"+spheredir)
    elif mac_or_mic=="Microscopic":
        radius=1    #in um
        length=200  #in cm
        databasekey="ThinCylinder"
        spheredir=""
        beamon=args.nEventMicro
        gpscmd="""/gps/particle neutron
        /gps/pos/type Point
        /gps/pos/centre 0. 0. """+str(-0.999*length/2)+""" cm
        /gps/direction 0 0 1
        /gps/ene/mono """

    app="/"+mac_or_mic+"_G4."+version+"-build"
    runcmd=applicationpath+app+"/"+mac_or_mic+" "+"myrun.mac > output.txt"
    exponent=int(math.log10(beamon))
    basepath=databasepath+"/Geant4/Geant4."+version+"/"+mac_or_mic.replace("scopic","")+"/"+spheredir

    with open(scriptpath+"/auxiliary/matarialdatabase.json") as jsonfile:
        database=json.load(jsonfile)
        materials=database[databasekey]

    if(not(os.path.isdir(basepath+librarydir))):
        os.system("mkdir "+basepath+librarydir)
    basepath=basepath+librarydir
    for material in materials:
        if material["TSLibrary"]!=TSLibrary:
            continue
        if mac_or_mic=="Macroscopic":
            newdir=(librarydir+"_"+material["material"]+"_"+str(radius)+"CM_"+
            str(material["temperature"])+"K_"+str(material["energy"])+"eV")
        elif mac_or_mic=="Microscopic":
            newdir=(librarydir+"_"+material["material"]+"_"+
            str(material["temperature"])+"K_"+str(material["energy"])+"eV")
        if(not(os.path.isdir(basepath+"/"+newdir))):
            os.system("mkdir "+basepath+"/"+newdir)
        newdir=newdir+"/1E"+str(exponent)
        if not(os.path.isdir(basepath+"/"+newdir)):
            os.system("mkdir "+basepath+"/"+newdir)
        workingdir=basepath+"/"+newdir
        if not(os.path.exists(workingdir+"/myrun.mac")):
            with open(workingdir+"/myrun.mac",'w') as myrunfile:
                myrunfile.write(verboscmd+'\n')
                myrunfile.write(delimiter)
                if numofthreads>0:
                    myrunfile.write(numofthreadscmd+str(numofthreads)+'\n')
                    myrunfile.write(delimiter)
                if radius!=30 and mac_or_mic=="Macroscopic":
                    myrunfile.write(detsizecmd+str(radius)+" cm"+'\n')
                if mac_or_mic=="Microscopic":
                    if radius!=1:
                        myrunfile.write(detradiuscmd+str(radius)+" um"+'\n')
                    if length!=200:
                        myrunfile.write(detlengthcmd+str(length)+" cm"+'\n')
                myrunfile.write(detmatcmd+material["G4_name"]+" "+
                str(material["density"])+" g/cm3 "+
                str(material["temperature"])+" kelvin "+
                str(material["pressure"])+" atmosphere"+'\n')
                myrunfile.write(delimiter)
                myrunfile.write(initializecmd+'\n')
                myrunfile.write(delimiter)
                myrunfile.write(particlehpcmd1+'\n')
                myrunfile.write(particlehpcmd2+'\n')
                myrunfile.write(delimiter)
                myrunfile.write(gpscmd+str(1.00001*material["energy"])+" eV"+'\n')
                myrunfile.write(delimiter)
                myrunfile.write(runcmd1+str(int(beamon/10))+'\n')
                myrunfile.write(runcmd2+str(int(beamon)))
        if not(os.path.exists(basepath+"/"+newdir+"/Results.root")):
            os.chdir(workingdir)
            print("\n\nCalculating in "+workingdir+"\n"+"...")
            os.system(runcmd)
            print("Simulation for "+newdir+" finished.")

print("\n\nJOB DONE!")
exit()
